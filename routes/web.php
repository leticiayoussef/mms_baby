<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');

    //Route::group(['middleware' => ['access:1']], function () {
    //listagem de usuarios
    Route::get('usuarios', 'UsuarioController@index');
    //lista o json dos usuarios
    Route::get('usuarios/listarJson', 'UsuarioController@listarJson');

    //add novo usuario
    Route::get('usuarios/adicionar', 'UsuarioController@frmAdd');
    Route::post('usuarios/adicionar', 'UsuarioController@adicionar');

    //editar usuario
    Route::get('usuarios/{id}/editar', 'UsuarioController@frmEditar');
    Route::post('usuarios/{id}/editar', 'UsuarioController@editar');
    //consultar email
    Route::post('usuarios/checar-email', 'UsuarioController@checarEmail');

    //deletar um usuario
    Route::post('usuarios/deletar', 'UsuarioController@deletar');

    //resetar Senha
    Route::any('usuarios/resetarSenha', 'UsuarioController@resetarSenha');

        //detalhar usuarios
    Route::any('usuarios/{id}', 'UsuarioController@detalhar');



//});
//
//Route::group(['middleware' => ['access:2']], function () {

    //listagem de clientes
    Route::get('clientes', 'Clientes\ClienteController@index');
    //lista o json dos clientes
    Route::get('clientes/listarJson', 'Clientes\ClienteController@listarJson');
    //detalhar cliente
    Route::get('clientes/{id}/detalhes', 'Clientes\ClienteController@detalhar');
    //adicionar novo cliente
    Route::get('clientes/adicionar', 'Clientes\ClienteController@frmAdd');
    Route::post('clientes/adicionar', 'Clientes\ClienteController@adicionar');
    //deletar um cliente
    Route::post('clientes/deletar', 'Clientes\ClienteController@deletar');
    //editar um cliente
    Route::get('clientes/{id}/editar', 'Clientes\ClienteController@frmEditar');
    Route::post('clientes/{id}/editar', 'Clientes\ClienteController@editar');

//});

//Route::group(['middleware' => ['access:3']], function () {
    //listagem de produto
    Route::get('produtos', 'Produto\ProdutoController@index');
    //lista o json dos produto
    Route::get('produtos/listarJson', 'Produto\ProdutoController@listarJson');
    //detalhar produto
    Route::get('produtos/{idproduto}/detalhes', 'Produto\ProdutoController@detalhar');
    //adicionar novo produto
    Route::get('produtos/adicionar', 'Produto\ProdutoController@frmAdd');
    Route::post('produtos/adicionar', 'Produto\ProdutoController@adicionar');
    //deletar um produto
    Route::post('produtos/deletar', 'Produto\ProdutoController@deletar');
    //editar um produto
    Route::get('produtos/{id}/editar', 'Produto\ProdutoController@frmEditar');
    Route::post('produtos/{id}/editar', 'Produto\ProdutoController@editar');

//});

    //listagem de produto
    Route::get('configuracoes/tipo_tamanho', 'TipoTamanho\TipoTamanhoController@index');
    //lista o json dos produto
    Route::get('configuracoes/tipo_tamanho/listarJson', 'TipoTamanho\TipoTamanhoController@listarJson');
    //detalhar produto
    Route::get('configuracoes/tipo_tamanho/{idproduto}/detalhes', 'TipoTamanho\TipoTamanhoController@detalhar');
    //adicionar novo produto
    Route::get('configuracoes/tipo_tamanho/adicionar', 'TipoTamanho\TipoTamanhoController@frmAdd');
    Route::post('configuracoes/tipo_tamanho/adicionar', 'TipoTamanho\TipoTamanhoController@adicionar');
    //deletar um produto
    Route::post('configuracoes/tipo_tamanho/deletar', 'TipoTamanho\TipoTamanhoController@deletar');
    //editar um produto
    Route::get('configuracoes/tipo_tamanho/{idproduto}/editar', 'TipoTamanho\TipoTamanhoController@frmEditar');
    Route::post('configuracoes/tipo_tamanho/{idproduto}/editar', 'TipoTamanho\TipoTamanhoController@editar');

    //listagem de produto
    Route::get('fornecedor', 'Fornecedor\FornecedorController@index');
    //lista o json dos produto
    Route::get('fornecedor/listarJson', 'Fornecedor\FornecedorController@listarJson');
    //detalhar produto
    Route::get('fornecedor/{idproduto}/detalhes', 'Fornecedor\FornecedorController@detalhar');
    //adicionar novo produto
    Route::get('fornecedor/adicionar', 'Fornecedor\FornecedorController@frmAdd');
    Route::post('fornecedor/adicionar', 'Fornecedor\FornecedorController@adicionar');
    //deletar um produto
    Route::post('fornecedor/deletar', 'Fornecedor\FornecedorController@deletar');
    //editar um produto
    Route::get('fornecedor/{id}/editar', 'Fornecedor\FornecedorController@frmEditar');
    Route::post('fornecedor/{id}/editar', 'Fornecedor\FornecedorController@editar');