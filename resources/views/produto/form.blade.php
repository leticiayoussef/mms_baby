@extends('layouts.app')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('produtos')}}">Produtos</a></li>
                        <li class="breadcrumb-item active">
                            @if(isset($produto->idprouto))
                                Editar
                            @else
                                Cadastrar
                            @endif
                            Produto
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="x_content">
        <div class="col-md-12">
            <form method="POST" enctype="multipart/form-data" name="frmTamanho"
                  action="{{url('produtos/adicionar')}}"
            >
                @include('parts.messages')
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Código do Produto</label>
                                <input type="text" name="produto[codigo_produto]" class="form-control"  id="codigo_produto" maxlength="10"
                                       placeholder="Digite o Cód Produto" value="@inputValue(old('produto.codigo_produto'), $produto->codigo_produto)">
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                                <label>Produto</label>
                                <input name="produto[descricao]" class="form-control" required id="descricao"
                                       placeholder="Digite nome do Produto" value="{{$produto->descricao}}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Valor Unitário</label>
                                <input name="produto[valor_unitario]" placeholder="R$0.00"
                                       required class="form-control valor"
                                       value="{{$produto->valor_unitario}}">

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Valor Venda</label>
                                <input name="produto[valor_venda]" class="form-control" placeholder="R$0.00"
                                       required class="form-control valor"
                                       value="{{$produto->valor_venda}}">

                            </div>
                        </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Tamanho</label>
                                    <select name="tipo_tamanho[idtipo_tamanho]" id="idtipo_tamanho"
                                            class="form-control">
                                        <option value="">Selecione</option>
                                        @foreach($tipo_tamanho as $t)
                                            <option value="{{$t->idtipo_tamanho}}"
                                                    @isset($ta->idtipo_tamanho)
                                                    @if($t->idtipo_tamanho == $ta->idtipo_tamanho)
                                                    selected
                                                    @endif
                                                    @endisset
                                            >
                                                {{$t->descricao}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                         {{--<div class="col-md-2">--}}
                            {{--<div class="form-group">--}}
                                {{--<label>Tamanho</label>--}}
                                {{--<select class="form-control" name="tipo_tamanho[$tipo_tamanho]">--}}
                                    {{--<option disabled> Selecione o Tamanho</option>--}}
                                    {{--@options($tipo_tamanho, 'idtipo_tamanho', 'descricao',--}}
                                    {{--App\Models\Utils::inputValue(old('tipo_tamanho.idtipo_tamanho'),--}}
                                    {{--$tipo_tamanho->idtipo_tamanho))--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        {{--</div>--}}


                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Estoque Atual</label>
                                <input name="produto[estoque_minimo]" class="form-control" required id="estoque_minimo" placeholder="10"
                                       value="{{$produto->estoque_minimo}}">
                            </div>
                        </div>
                        <div class="col-md-12 text-right">
                            {{ csrf_field() }}
                            <a href="{{url('produtos')}}">
                                <button type="button" class="btn btn-default">Voltar</button>
                            </a>
                            @if(isset($produto->idproduto))
                                <button type="submit" id="btnEditar" name="btnEditar" class="btn btn-primary">
                                    Editar
                                </button>
                            @else
                                <button type="submit" id="btnCadastrar" name="btnCadastrar" class="btn btn-primary">
                                    Cadastrar
                                </button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
    </div>
@endsection