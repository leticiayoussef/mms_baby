@extends('layouts.app')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('usuarios')}}">Usuários</a></li>
                        <li class="breadcrumb-item active">
                            @if(isset($usuario->id))
                                Editar
                            @else
                                Cadastrar
                            @endif
                            Usuário
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    @if(isset($usuario->id))
                        Editar
                    @else
                        Cadastrar
                    @endif
                    Usuário
                </h3>
            </div>
            <div class="box-body com-borda">
                <form enctype="multipart/form-data" id="frmUsuario" name="frmUsuario"
                      @if(isset($usuario->id))
                      action="{{url('usuarios/'.$usuario->id.'/editar')}}"
                      @else
                      action="{{url('usuarios/adicionar')}}"
                      @endif
                      method="POST">
                    @include('parts.messages')
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nome</label>
                                <input name="usuario[nome]" class="form-control" required
                                       value="@inputValue(old('usuario.nome'), $usuario->nome)">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>E-mail </label>
                                <input type="email" name="usuario[email]" class="form-control" required id="email"
                                       value="@inputValue(old('usuario.email'), $usuario->email)">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tipos de Usuário</label>
                                <select class="form-control" name="usuario[idtipo_usuario]">
                                    <option disabled> Selecione o Tipo</option>
                                    @options($tipos_usuarios, 'idtipo_usuario', 'descricao',
                                    App\Models\Utils::inputValue(old('usuario.idtipo_usuario'),
                                    $usuario->idtipo_usuario))
                                </select>
                            </div>
                        </div>
                        @if(!isset($usuario->id))
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Senha </label>
                                <input type="password" name="usuario[senha]" class="form-control" required id="senha">
                            </div>
                        </div>
                        @endif
                        <div class="col-md-12 text-right">
                            {{ csrf_field() }}
                            <a href="{{url('usuarios')}}">
                                <button type="button" class="btn btn-default">Voltar</button>
                            </a>
                            @if(isset($usuario->id))
                                <input style="display: none;" id="id" type="text" name="usuario[id]" value="{{$usuario->id}}">
                                <button type="button" id="btnResetarSenha" name="btnResetarSenha" class="btn btn-info">
                                    Resetar Senhar
                                </button>
                                <button type="button" id="btnEditar" name="btnEditar" class="btn btn-primary">
                                    Editar
                                </button>
                            @else
                                <button type="submit" id="btnCadastrar" name="btnCadastrar" class="btn btn-primary">
                                    Cadastrar
                                </button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script>
        $(document).ready(function () {
            $('#btnEditar').on('click', function () {
                var _this = this;
                swal({
                    title: "Você tem certeza?",
                    text: "Você realmente deseja editar este Usuário?",
                    icon: "warning",
                    dangerMode: true,
                    buttons: {
                        cancel: "Cancelar",
                        confirm: {
                            text: "Sim",
                            value: "1",
                        },
                    },
                }).then(function (value) {
                    // alert(value);
                    if (value == 1) {
                        swal({
                            icon: "success"
                        });
                        document.frmUsuario.submit();
                        $("#btnEditar").attr("disabled", "disabled");
                    } else {
                        $(this).closest('swal-modal').remove();
                    }
                });
            });

            $("#btnResetarSenha").click(function(){
                swal({
                    text: "Digite a nova senha para o usuário: ",
                    content: "input",
                    icon: "warning",
                    dangerMode: true,
                    buttons: [
                        "Cancelar",
                        {
                            text: "Redefinir senha",
                            type: "password",
                            className: 'btn-primary'
                        }
                    ]
                }).then(function(senha){
                    var senha = senha;
                    if(senha) {
                        $.ajax({
                            url: BASE_URL + 'usuarios/resetarSenha',
                            type: 'post',
                            data: {
                                id: $("#id").val(),
                                senha: senha
                            },
                            dataType: 'JSON',
                            success: function (result)
                            {
                                swal({
                                    title: result,
                                    icon: "success"
                                });
                            },
                            error: function (result) {
                                swal({
                                    title: "Erro ao redefinir senha",
                                    text: result.responseJSON.message,
                                    icon: "error"
                                });
                            }
                        });
                    }
                });
            });

        });
    </script>
@endsection
