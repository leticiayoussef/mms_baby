@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('usuarios')}}">Usuários</a></li>
                        <li class="breadcrumb-item active">Detalhes do Usuário</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Main content -->
    <div class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Detalhes do Usuário </h3>
            </div>
            <div class="box-body com-borda">
                 <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Nome</label>
                                <input name="nome" class="form-control" disabled
                                       value="{{$usuario->nome}}">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>E-mail </label>
                                <input name="email" class="form-control" disabled id="email"
                                       value="{{$usuario->email}}">
                            </div>
                        </div>
                     <div class="col-md-3">
                         <div class="form-group">
                             <label>Tipo de Usuário</label>
                             <input name="tipos_usuarios" class="form-control" disabled
                                    value="{{$usuario->tipos_usuarios->descricao}}">
                         </div>
                     </div>



                        <div class="col-md-12 text-right">
                            {{csrf_field()}}
                            <a href="{{url('usuarios')}}">
                                <button type="button" class="btn btn-default">
                                    Voltar
                                </button>
                            </a>
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection
