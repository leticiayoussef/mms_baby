@extends('layouts.app')
@if(isset($fornecedor->idfornecedor))
    @section('pageTitle', 'Editar Fornecedor')
@else
    @section('pageTitle', 'Adicionar Fornecedor')
@endif
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('fornecedor')}}">Fornecedor</a></li>
                        <li class="breadcrumb-item active">
                            @if(isset($fornecedor->idfornecedor))
                                Editar
                            @else
                                Adicionar
                            @endif
                            Fornecedor
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="box box-primary">
            <div class="col-md-12">
                <div class="form-group">
                    <br>
                    <h4>
                        <center>
                            Cadastrar Fornecedor
                        </center>
                    </h4>
                    <hr>
                </div>
            </div>
            <div class="box-body com-borda">
                <form method="POST" enctype="multipart/form-data" name="frmFornecedor"
                      @if(isset($fornecedor->idfornecedor))
                      action="{{url('fornecedor/'.$fornecedor->idfornecedor.'/editar')}}"
                      @else
                      action="{{url('fornecedor/adicionar')}}"
                        @endif>
                    <div class="col-md-12">
                        <div class="row margin-b-10">
                            @include('parts.messages')
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Razão Social *</label>
                                    <input name="fornecedor[razao_social]" class="form-control" required id="razao_social"
                                           value="{{$fornecedor->razao_social}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>E-mail*</label>
                                    <input name="fornecedor[email]" class="form-control" required
                                           id="email"
                                           value="{{$fornecedor->email}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Site</label>
                                    <input name="fornecedor[site]" class="form-control" required
                                           id="site"
                                           value="{{$fornecedor->site}}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>CNPJ*</label>
                                    <input name="fornecedor[cnpj]" class="form-control" required id="cnpj"
                                           value="{{$fornecedor->cnpj}}"
                                           maxlength="14" minlength="14">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Telefone*</label>
                                    <input type="text" maxlength="10" minlength="10"
                                           name="fornecedor[telefone]"
                                           class="telefone form-control"
                                           value="{{old('fornecedor.telefone')}}"
                                           pattern="\([0-9]{2}\) [0-9]{4,6}-[0-9]{3,4}" id="telefone">
                                    <span id="span_telefone" style="color: red; display: none;">Quantidade mínima de números não atingida!</span>
                                </div>
                            </div>


                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Inscrição Estadual</label>
                                    <input name="fornecedor[inscricao_estadual]" class="form-control" required
                                           id="inscricao_estadual" maxlength="12" minlength="12"
                                           value="{{old('fornecedor.inscricao_estadual')}}">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <br>
                                    <h5>
                                        Endereço
                                    </h5>
                                    <hr>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>CEP*
                                        <a href="http://www.buscacep.correios.com.br/servicos/dnec/index.do"
                                           target="_blank"><i class="fa fa-map-marker fa-lg"
                                                              title="Consultar no Site do Correios"
                                                              aria-hidden="true"></i></a></label>
                                    <input name="funcionario_endereco[cep]" class="cep form-control" type="text"
                                           id="cep"
                                           placeholder="CEP" value="{{old('funcionario_endereco.cep')}}" required/>
                                </div>
                            </div>

                            <div class="col-md-9">
                                <div class="form-group">
                                    <label>Endereço*</label>
                                    <input name="funcionario_endereco[logradouro]" class="form-control"
                                           type="text" id="logradouro" placeholder="Logradouro"
                                           value="{{old('funcionario_endereco.logradouro')}}" required/>
                                </div>
                            </div>

                            <div class="col-md-1">
                                <div class="form-group">
                                    <label>Número*</label>
                                    <input name="funcionario_endereco[numero]"
                                           value="{{old('funcionario_endereco.numero')}}"
                                           class="form-control" placeholder="Nº"
                                           required>
                                </div>
                            </div>

                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Complemento</label>
                                    <input name="funcionario_endereco[complemento]"
                                           placeholder="Apto, casa, proximidade, etc"
                                           value="{{old('funcionario_endereco.complemento')}}"
                                           class="form-control">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Bairro*</label>
                                    <input name="funcionario_endereco[bairro]" placeholder="Bairro"
                                           value="{{old('funcionario_endereco.bairro')}}" id="bairro"
                                           class="form-control" type="text" size="40"/>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Cidade*</label>
                                    <input name="funcionario_endereco[cidade]" placeholder="Cidade"
                                           value="{{old('funcionario_endereco.cidade')}}" type="text"
                                           class="form-control" id="cidade" size="40" required=""/>
                                </div>
                            </div>

                            <div class="col-md-1">
                                <div class="form-group">
                                    <label>Estado*</label>
                                    <input name="funcionario_endereco[uf]" placeholder="UF"
                                           value="{{old('funcionario_endereco.uf')}}" type="text"
                                           class="form-control" id="uf" size="2" required=""/>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <br>
                            </div>
                            <input type="hidden" name="status" value="1">
                            <div class="col-md-12 text-right">
                                {{ csrf_field() }}
                                <a href="{{url('funcionario')}}">
                                    <button type="button" class="btn btn-default">Voltar</button>
                                </a>
                                @if(isset($fornecedor->idfornecedor))
                                    <input type="hidden" name="id" value="{{$fornecedor->idfornecedor}}">
                                    <button type="button" id="btnEditar" name="btnEditar"
                                            class="btn btn-primary">
                                        Editar
                                    </button>
                                @else
                                    <button type="button" id="btnCadastrar" name="btnCadastrar"
                                            class="btn btn-primary">
                                        Cadastrar
                                    </button>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script src="{{asset('js/Utils/ConsultarCep.js')}}"></script>
    <script src="{{asset('js/Utils/EstadosCidadesBairros.js')}}"></script>
    <script src="{{asset('js/Utils/HabilitarCampos.js')}}"></script>
    <script>

        $(document).ready(function () {
            $("#cep").mask("99999-999");
            $("#cnpj").mask("99.999.999/9999-99");

            $('#btnEditar').on('click', function () {
                var _this = this;
                var idfornecedor = $("#id");
                swal({
                    title: "Você tem certeza?",
                    text: "Você realmente deseja editar este funcionario?",
                    icon: "warning",
                    buttons: {
                        cancel: "Cancelar",
                        confirm: {
                            text: "Sim",
                            value: "1",
                        },

                    },
                }).then(function (value) {
                    // alert(value);
                    if (value == 1) {
                        document.frmFornecedor.submit();
                    } else {
                        $(this).closest('swal-modal').remove();
                    }
                });
            });


            $("#btnCadastrar").on('click', function () {
                document.frmFornecedor.submit();
                $("#btnCadastrar").attr("disabled", "disabled");
            });
            $("#btnEditar").on('click', function () {
                document.frmFornecedor.submit();
                $("#btnEditar").attr("disabled", "disabled");
            });

            var SPMaskBehavior = function (val) {
                    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
                },
                spOptions = {
                    onKeyPress: function (val, e, field, options) {
                        field.mask(SPMaskBehavior.apply({}, arguments), options);
                    }
                };
            $('.telefone').mask(SPMaskBehavior, spOptions);
            $("#telefone").on('keyup', function () {
                var telefone = $('#telefone').val().length;
                if (telefone >= 1 && telefone < 10) {
                    $("#telefone").css('border', '1px solid red');
                    $("#span_telefone").show();
                } else {
                    $("#telefone").css('border', '1px solid #ccc');
                    $("#span_telefone").hide();
                }
            });
        });

    </script>
@endsection
