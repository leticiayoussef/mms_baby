@extends('layouts.app')
@section('pageTitle', 'Listagem de Fornecedor')
@section('content')
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        {{--<li><a href="{{url('hoteis')}}"> Clientes </a></li>--}}
    </ol>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h3>Listagem de Fornecedor</h3>
                <div class="clearfix"></div>
            </div>
            <br>
            <div class="x_content">
                <div class="box-body">
                    <div class="row margin-b-10">
                        <div class="col-md-10">
                            <a href="{{url('fornecedor/adicionar')}}">
                                <button class="btn btn-success">Adicionar Fornecedor</button>
                            </a>
                        </div>
                    </div>
                </div>
                <br>
                            @include('parts.messages')

                            <div class="row mt-10">
                                <div class="col-md-12">
                                    <table class="table table-bordered responsive" id="tblFornecedor">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Razão Social</th>
                                            <th>Nome Fantasia</th>
                                            <th>CNPJ</th>
                                            <th>Telefone</th>
                                            <th>Ferramentas</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script>
        $("#tblFornecedor").DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": BASE_URL + "fornecedor/listarJson",
            "columns": [
                {"data": "idfornecedor", "name": "idfornecedor"},
                {"data": "razao_social", "name": "razao_social"},
                {"data": "nome_fantasia", "name": "nome_fantasia"},
                {"data": "cnpj", "name": "cnpj",
                    "render": function(data){
                        if(data == null){
                            return " Sem CNPJ";
                        }else{
                            return data;
                        }
                    }
                },
                {"data": "telefone", "name": "telefone"},
                {"data": null, "orderable": false}
            ],
            "order": [[0, "desc"]],
            "columnDefs": [
                {
                    numberColumn:'dt-center',
                    "targets": 5,
                    "searchable": false,
                    "data": "idfornecedor",
                    "render": function (data, type, row, meta) {
                        html = '<a href="fornecedor/' + data.idfornecedor + '/detalhes">' +
                            '<button type="button" class="btn btn-sm btn-info">' +
                            '<i class="fa fa-search fa-lg" aria-hidden="true" title="Listar"></i>' +
                            '</button>' +
                            '</a> ' +
                            '<a href="fornecedor/' + data.idfornecedor + '/editar">' +
                            '<button type="button" class="btn btn-sm btn-primary">' +
                            '<i class="fa fa-pencil fa-lg" aria-hidden="true" title="Editar"></i>' +
                            '</button> ' +
                            '</a>' +
                            '<button type="button" class="btn btn-sm btn-danger btnDeletar" data-id="' + data.idfornecedor + '">' +
                            '<i class="fa fa-times fa-lg" aria-hidden="true" title="Deletar"></i>' +
                            '</button>';

                        return html;
                    }
                }
            ],

            lengthMenu:[25,50,100],
            dom: 'lBfrtip',

            buttons: [
                {
                    extend: 'excel',
                    exportOptions:{columns:[0,1,2]},
                    text:
                        '<i class="fa fa-file-excel-o"><font face="Calibri"> Excel</font></i>',
                },
                {
                    extend: 'pdf',
                    exportOptions:{columns:[0,1,2]},
                    text:
                        '<i class="fa fa-file-pdf-o"><font face="Calibri"> PDF</font></i>',
                },
            ],

            "deferRender": true,
            "language": {
                "decimal": "",
                "emptyTable": "Nenhum conteúdo disponível na tabela",
                "info": "Mostrando _START_ até _END_ de _TOTAL_ itens",
                "infoEmpty": "Mostrando 0 até 0 de 0 itens",
                "infoFiltered": "(Mostrando _MAX_ itens)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ itens",
                "loadingRecords": "Carregando...",
                "processing": "Processando...",
                "search": "Pesquisar:",
                "zeroRecords": "Nenhum item encontrado",
                "paginate": {
                    "first": "Primeiro",
                    "last": "Último",
                    "next": "Próximo",
                    "previous": "Anterior"
                },
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        });

        $('#tblFornecedor tbody').on('click', '.btnDeletar', function () {
            var _this = this;
            var idfornecedor = $(_this).attr('data-id')
            swal({
                title: "Você tem certeza?",
                text: "Você realmente deseja deletar esta fornecedor?",
                icon: "warning",
                dangerMode: true,
                buttons: [
                    "Cancelar", "Sim, deletar"
                ],
            }).then(function (result) {
                if (result) {
                    $.ajax({
                        url: BASE_URL + 'fornecedor/'+idfornecedor+'/deletar',
                        type: 'POST',
                        data: {
                            idfornecedor_produto: $(_this).attr('data-id')
                        },
                        dataType: 'JSON',
                        success: function (result) {
                            if (result == 1) {
                                swal({
                                    title: "Deletado com sucesso",
                                    icon: "success"
                                });
                                $(_this).closest('tr').remove();
                            }
                            else {
                                swal({
                                    title: "Erro ao deletar a fornecedor",
                                    icon: "error"
                                });
                            }
                        },
                        error: function (result) {
                            swal({
                                title: "Erro ao deletar a fornecedor",
                                text: result.responseJSON.message,
                                icon: "error"
                            });
                        }
                    });
                }
            });
        });
    </script>
@endsection
@section('css')
    <style>
        .div-botao {
            padding-bottom: 10px;
        }
    </style>
@endsection