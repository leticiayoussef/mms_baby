@extends('layouts.app')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('tipo_tamanho')}}">Tipo de Tamanho</a></li>
                        <li class="breadcrumb-item active">
                            @if(isset($usuario->id))
                                Editar
                            @else
                                Cadastrar
                            @endif
                            Tipo de Tamanho
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    @if(isset($usuario->id))
                        Editar
                    @else
                        Cadastrar
                    @endif
                    Tipo de Tamanho
                </h3>
            </div>
            <div class="box-body com-borda">
                <form enctype="multipart/form-data" id="frmTamanho" name="frmTamanho"
                      @if(isset($usuario->id))
                      action="{{url('configuracoes/tipo_tamanho/'.$tipo_tamanho->idtipo_tamanho.'/editar')}}"
                      @else
                      action="{{url('configuracoes/tipo_tamanho/adicionar')}}"
                      @endif
                      method="POST">
                    @include('parts.messages')
                    <div class="row">
                        <
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Tipo de Tamanho </label>
                                <input type="text" name="tipo_tamanho[descricao]" class="form-control" required id="descricao"
                                       value="@inputValue(old('tipo_tamanho.descricao'), $tipo_tamanho->descricao)">
                            </div>
                        </div>

                        <div class="col-md-12 text-right">
                            {{ csrf_field() }}
                            <a href="{{url('configuracoes/tipo_tamanho')}}">
                                <button type="button" class="btn btn-default">Voltar</button>
                            </a>
                            @if(isset($tipo_tamanho->idtipo_tamanho))
                                <input style="display: none;" id="id" type="text" name="tipo_tamanho[idtipo_tamanho]" value="{{$tipo_tamanho->idtipo_tamanho}}">
                                <button type="button" id="btnEditar" name="btnEditar" class="btn btn-primary">
                                    Editar
                                </button>
                            @else
                                <button type="submit" id="btnCadastrar" name="btnCadastrar" class="btn btn-primary">
                                    Cadastrar
                                </button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script>
        $(document).ready(function () {
            $('#btnEditar').on('click', function () {
                var _this = this;
                swal({
                    title: "Você tem certeza?",
                    text: "Você realmente deseja editar este Usuário?",
                    icon: "warning",
                    dangerMode: true,
                    buttons: {
                        cancel: "Cancelar",
                        confirm: {
                            text: "Sim",
                            value: "1",
                        },
                    },
                }).then(function (value) {
                    // alert(value);
                    if (value == 1) {
                        swal({
                            icon: "success"
                        });
                        document.frmUsuario.submit();
                        $("#btnEditar").attr("disabled", "disabled");
                    } else {
                        $(this).closest('swal-modal').remove();
                    }
                });
            });

            $("#btnResetarSenha").click(function(){
                swal({
                    text: "Digite a nova senha para o usuário: ",
                    content: "input",
                    icon: "warning",
                    dangerMode: true,
                    buttons: [
                        "Cancelar",
                        {
                            text: "Redefinir senha",
                            type: "password",
                            className: 'btn-primary'
                        }
                    ]
                }).then(function(senha){
                    var senha = senha;
                    if(senha) {
                        $.ajax({
                            url: BASE_URL + 'usuarios/resetarSenha',
                            type: 'post',
                            data: {
                                id: $("#id").val(),
                                senha: senha
                            },
                            dataType: 'JSON',
                            success: function (result)
                            {
                                swal({
                                    title: result,
                                    icon: "success"
                                });
                            },
                            error: function (result) {
                                swal({
                                    title: "Erro ao redefinir senha",
                                    text: result.responseJSON.message,
                                    icon: "error"
                                });
                            }
                        });
                    }
                });
            });

        });
    </script>
@endsection
