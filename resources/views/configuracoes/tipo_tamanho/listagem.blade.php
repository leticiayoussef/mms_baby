@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item active">Usuários</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Listagem de Usuários</h3>
                    </div>

                    <div class="box-body">
                        <div class="row margin-b-10">
                            <div class="col-md-10">
                                <a href="{{url('tamanho/adicionar')}}">
                                    <button class="btn btn-sm btn-success">Adicionar Tamanho</button>
                                </a>
                            </div>

                            <div class="col-md-12 com-borda">
{{--                                @include('parts.messages')--}}
                                <table class="table table-bordered" id="tblTamanho">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Descrição</th>
                                        <th>Ferramentas</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script>
        $("#tblUsuarios").DataTable({
            "responsive": true,
            "order": [[0, 'desc']],
            lengthMenu:[25,50,100],
            "processing": true,
            "serverSide": true,
            "ajax": BASE_URL + "tamanho/listarJson",
            "columns": [
                { "data" : "id", "name": "id" },
                { "data": "descricao", "name": "descricao" },
                { "data": null, "orderable": false }
            ],
            "columnDefs": [
                {
                    "targets": 2,
                    "searchable": false,
                    "data": "idtipo_tamanho",
                    "render": function ( data, type, row, meta ) {
                        html = '<a href="'+BASE_URL+'tamanho/'+data.idtipo_tamanho+'">'+
                            '<button type="button" class="btn btn-sm btn-info">'+
                            '<i class="fa fa-search fa-lg" aria-hidden="true" title="Listar"></i>'+
                            '</button>'+
                            '</a> ' +
                            '<a href="'+BASE_URL+'tamanho/'+data.idtipo_tamanho+'/editar">'+
                            '<button type="button" class="btn btn-sm btn-primary">'+
                            '<i class="fa fa-pencil fa-lg" aria-hidden="true" title="Editar"></i>'+
                            '</button> ' +
                            '</a>'+
                            '<button type="button" class="btn btn-sm btn-danger btnDeletar" data-id="'+data.idtipo_tamanho+'">'+
                            '<i class="fa fa-times fa-lg" aria-hidden="true" title="Deletar"></i>'+
                            '</button>';

                        return html;
                    }
                }
            ],
            dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excel',
                    exportOptions:{columns:[0,1,2]},
                    text:
                        '<i class="fa fa-file-excel-o"><font face="Calibri"> Excel</font></i>',
                },
                {
                    extend: 'pdf',
                    exportOptions:{columns:[0,1,2]},
                    text:
                        '<i class="fa fa-file-pdf-o"><font face="Calibri"> PDF</font></i>',
                },
            ],
            "deferRender": true,
            "language": {
                "decimal":        "",
                "emptyTable":     "Nenhum conteúdo disponível na tabela",
                "info":           "Mostrando _START_ até _END_ de _TOTAL_ itens",
                "infoEmpty":      "Mostrando 0 até 0 de 0 itens",
                "infoFiltered":   "(Mostrando _MAX_ itens)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ itens",
                "loadingRecords": "Carregando...",
                "processing":     "Processando...",
                "search":         "Pesquisar:",
                "zeroRecords":    "Nenhum item encontrado",
                "paginate": {
                    "first":      "Primeiro",
                    "last":       "Último",
                    "next":       "Próximo",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        } );

        $('#tblTamanho tbody').on( 'click', '.btnDeletar', function () {
            var _this = this;
            swal({
                title: "Você tem certeza?",
                text: "Você realmente deseja deletar esse Tamanho?",
                icon: "warning",
                dangerMode: true,
                buttons: [
                    "Cancelar", "Sim, deletar"
                ],
            }).then(function(result){
                if (result) {
                    $.ajax({
                        url: BASE_URL + 'tamanho/deletar',
                        type: 'POST',
                        data: {
                            id: $(_this).attr('data-id')
                        },
                        dataType: 'JSON',
                        success: function(result){
                            if(result == 1) {
                                swal({
                                    title: "Deletado com sucesso",
                                    icon: "success"
                                });
                                $(_this).closest('tr').remove();
                            }
                            else{
                                swal({
                                    title: "Erro ao deletar tamanho",
                                    icon: "error"
                                });
                            }
                        },
                        error: function(result){
                            swal({
                                title: "Erro ao deletar tamanho",
                                text: result.responseJSON.message,
                                icon: "error"
                            });
                        }
                    });
                }
            });
        });
    </script>
@endsection
