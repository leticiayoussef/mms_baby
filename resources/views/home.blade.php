@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Olá {{ Auth::user()->nome }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="{{route('home')}}">Home</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    {{--    <div class="content" >--}}
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="card card-primary card-outline">
                    <div class="card-body">
                        <h5 class="card-title1">Produtos</h5>

                        {{--<p class="card-text">--}}
                        {{--grafico de usuarios--}}
                        {{--</p>--}}
                        <a href="{{url('produtos')}}" class="card-link">Listagem de Produtos</a>

                    </div>
                </div><!-- /.card -->
            </div>
            <div class="col-lg-6">
                <div class="card card-primary card-outline">
                    <div class="card-body">
                        <h5 class="card-title1">Fornecedor</h5>

                        {{--<p class="card-text">--}}
                        {{--grafico de clientes--}}
                        {{--</p>--}}
                        <a href="{{url('fornecedor')}}" class="card-link">Listagem de Fornecedores</a>

                    </div>
                </div><!-- /.card -->
            </div>
            <div class="col-lg-6">
                <div class="card card-primary card-outline">
                    <div class="card-body">
                        <h5 class="card-title1">Clientes</h5>

                        {{--<p class="card-text">--}}
                        {{--grafico de clientes--}}
                        {{--</p>--}}
                        <a href="{{url('clientes')}}" class="card-link">Listagem de Clientes</a>

                    </div>
                </div><!-- /.card -->
            </div>
            <div class="col-lg-6">
                <div class="card card-primary card-outline">
                    <div class="card-body">
                        <h5 class="card-title1">Estoque</h5>

                        {{--<p class="card-text">--}}
                        {{--grafico de clientes--}}
                        {{--</p>--}}
                        <a href="{{url('estoque')}}" class="card-link">Listagem de Estoque</a>

                    </div>
                </div><!-- /.card -->
            </div>

            <!-- /.col-md-6 -->
        {{--                <div class="col-lg-6">--}}

        {{--                    <div class="card card-primary card-outline">--}}
        {{--                        <div class="card-header">--}}
        {{--                            <h5 class="m-0">Featured</h5>--}}
        {{--                        </div>--}}
        {{--                        <div class="card-body">--}}
        {{--                            <h6 class="card-title">Special title treatment</h6>--}}

        {{--                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>--}}
        {{--                            <a href="#" class="btn btn-primary">Go somewhere</a>--}}
        {{--                        </div>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
    {{--    </div>--}}
    <!-- /.content -->
@endsection
@section('css')
    <style>
        .row {
            display: flex;
            flex-wrap: wrap;
            margin-right: 0.5px;
            margin-left: 0.5px;
        }

        .card-title1 {
            margin-bottom: 7px !important;
        }
    </style>
@endsection
