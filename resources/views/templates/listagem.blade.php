@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item active">Módulo</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Main content -->
    <div class="content" >
        <div class="container-fluid">
            <div class="row">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Listagem de _</h3>
                    </div>

                    <div class="box-body">
                        <div class="row margin-b-10">
                            <div class="col-md-10">
                                <a href="{{url('clientes/adicionar')}}">
                                    <button class="btn btn-sm btn-success">Adicionar Cliente</button>
                                </a>
                            </div>

                            <div class="col-md-12 com-borda">
{{--                                @include('parts.messages')--}}
                                <table class="table table-bordered" id="tblUsuarios">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nome</th>
                                        <th>E-mail</th>
                                        <th>Ferramentas</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
