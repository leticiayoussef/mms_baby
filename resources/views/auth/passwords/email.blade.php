@extends('layouts.auth')

@section('content')
    <div class="card-body login-card-body">
        <p class="login-box-msg">Esqueci minha senha</p>

        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <div class="form-group has-feedback">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="E-mail">

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="row mb-1">
                <div class="col-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">
                        Enviar redefinição de senha
                    </button>
                </div>
            </div>
        </form>

        <p class="mb-0">
            <a href="{{route('login')}}" class="text-center">Voltar</a>
        </p>
    </div>
@endsection
