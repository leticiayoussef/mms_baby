@extends('layouts.auth')

@section('content')
    <div class="card-body login-card-body">
        <p class="login-box-msg">Preencha seus dados para se casastrar</p>

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div class="form-group has-feedback">
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Name">

                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="form-group has-feedback">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="E-mail">

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="form-group has-feedback">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Senha">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="form-group has-feedback">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirme a senha">
            </div>

            <div class="row">
                <div class="col-4 offset-8">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">
                        Cadastrar
                    </button>
                </div>
            </div>
        </form>

        <div class="social-auth-links text-center mb-3">
            <p>- OU -</p>
            <a href="#" class="btn btn-block btn-primary">
                <i class="fa fa-facebook mr-2"></i> Cadastrar com Facebook
            </a>
            <a href="#" class="btn btn-block btn-danger">
                <i class="fa fa-google-plus mr-2"></i> Cadastrar com o Google
            </a>
        </div>
        <!-- /.social-auth-links -->

        <p class="mb-0">
            <a href="{{route('login')}}" class="text-center">Já tenho cadastro</a>
        </p>
    </div>
    <!-- /.login-card-body -->
@endsection
