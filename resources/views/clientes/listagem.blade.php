@extends('layouts.app')
@section('pageTitle', 'Listagem de Clientes')
@section('content')
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li><a href="{{url('hoteis')}}"> Clientes </a></li>
    </ol>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Listagem de Clientes</h3>
                    <div class="clearfix"></div>
                </div>
                <br>
                <div class="x_content">
                    <div class="box-body">
                        <div class="row margin-b-10">
                            <div class="col-md-10">
                                <a href="{{url('clientes/adicionar')}}">
                                    <button class="btn btn-success">Adicionar Cliente</button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    {{--@include('parts.messages')--}}
                    <table class="table table-bordered" id="tblClientes">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nome</th>
                            <th>Celular</th>
                            <th>Ferramentas</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $("#tblClientes").DataTable({
            // "responsive": true,
            "order": [[0, 'desc']],
            lengthMenu:[25,50,100],
            "processing": true,
            "serverSide": true,
            "ajax": BASE_URL + "clientes/listarJson",
            "columns": [
                { "data" : "idcliente", "name": "idcliente" },
                { "data": "nome", "name": "nome",
                    'render': function (data) {
                        var nome = "---";

                        if (data == null) {
                            return nome;
                        } else {
                            return data;
                        }
                    }
                },
                { "data": "celular", "name": "celular"},
                { "data": null, "orderable": false }
            ],
            "columnDefs": [
                {
                    "targets": 3,
                    "searchable": false,
                    "data": "idcliente",
                    "render": function ( data, type, row, meta ) {
                        html = '<a href="'+BASE_URL+'clientes/'+data.idcliente+'/detalhes">'+
                            '<button type="button" class="btn btn-sm btn-info">'+
                            '<i class="fa fa-search fa-lg" aria-hidden="true" title="Listar"></i>'+
                            '</button>'+
                            '</a> ' +
                            '<a href="'+BASE_URL+'clientes/'+data.idcliente+'/editar">'+
                            '<button type="button" class="btn btn-sm btn-primary">'+
                            '<i class="fa fa-pencil fa-lg" aria-hidden="true" title="Editar"></i>'+
                            '</button> ' +
                            '</a>'+
                            '<button type="button" class="btn btn-sm btn-danger btnDeletar" data-id="'+data.idcliente+'">'+
                            '<i class="fa fa-times fa-lg" aria-hidden="true" title="Deletar"></i>'+
                            '</button>';

                        return html;
                    }
                }
            ],
            dom: 'lBfrtip',

            buttons: [
                {
                    extend: 'excel',
                    exportOptions:{columns:[0,1,2]},
                    text:
                        '<i class="fa fa-file-excel-o"><font face="Calibri"> Excel</font></i>',
                },
                {
                    extend: 'pdf',
                    exportOptions:{columns:[0,1,2]},
                    text:
                        '<i class="fa fa-file-pdf-o"><font face="Calibri"> PDF</font></i>',
                },
            ],

            "deferRender": true,
            "language": {
                "decimal":        "",
                "emptyTable":     "Nenhum conteúdo disponível na tabela",
                "info":           "Mostrando _START_ até _END_ de _TOTAL_ itens",
                "infoEmpty":      "Mostrando 0 até 0 de 0 itens",
                "infoFiltered":   "(Mostrando _MAX_ itens)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ itens",
                "loadingRecords": "Carregando...",
                "processing":     "Processando...",
                "search":         "Pesquisar:",
                "zeroRecords":    "Nenhum item encontrado",
                "paginate": {
                    "first":      "Primeiro",
                    "last":       "Último",
                    "next":       "Próximo",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        } );

        $('#tblClientes tbody').on( 'click', '.btnDeletar', function () {
            var _this = this;
            swal({
                title: "Você tem certeza?",
                text: "Você realmente deseja deletar esse Cliente?",
                icon: "warning",
                dangerMode: true,
                buttons: [
                    "Cancelar", "Sim, deletar"
                ],
            }).then(function(result){
                if (result) {
                    $.ajax({
                        url: BASE_URL + 'clientes/deletar',
                        type: 'POST',
                        data: {
                            idcliente: $(_this).attr('data-id')
                        },
                        dataType: 'JSON',
                        success: function(result){
                            if(result == 1) {
                                swal({
                                    title: "Deletado com sucesso",
                                    icon: "success"
                                });
                                $(_this).closest('tr').remove();
                            }
                            else{
                                swal({
                                    title: "Erro ao deletar cliente",
                                    icon: "error"
                                });
                            }
                        },
                        error: function(result){
                            swal({
                                title: "Erro ao deletar cliente",
                                text: result.responseJSON.message,
                                icon: "error"
                            });
                        }
                    });
                }
            });
        });
    </script>
@endsection
