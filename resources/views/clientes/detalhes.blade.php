@extends('layouts.app')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('clientes')}}">Clientes</a></li>
                        <li class="breadcrumb-item active">
                            @if(isset($produto->idprouto))
                                Editar
                            @else
                                Cadastrar
                            @endif
                            Cliente
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="x_content">
        <div class="col-md-12">
            <form method="POST" enctype="multipart/form-data" name="frmCliente"
                  action="{{url('clientes/adicionar')}}"
            >
                @include('parts.messages')
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Nome *</label>
                            <input id="nome" class="form-control" name="clientes[nome]" disabled
                                   value="{{$clientes->nome}}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Telefone *</label>
                            <input name="clientes[telefone]" class="form-control" disabled id="telefone"
                                   value="{{$clientes->telefone}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Celular *</label>
                            <input name="clientes[celular]" class="form-control" disabled id="celular"
                                   value="{{$clientes->celular}}">
                        </div>
                    </div>

                    {{--ENDEREÇO--}}

                    <div class="col-md-2">
                        <div class="form-group">
                            <label>CEP* <a href="http://www.buscacep.correios.com.br/servicos/dnec/index.do" target="_blank"><i class="fa fa-map-marker fa-lg" title="Consultar no Site do Correios" aria-hidden="true"></i></a></label>
                            <input name="clientes[cep]" class="form-control" type="text" id="cep"  value="{{$clientes->cep}}" disabled />
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <label>Endereço*</label>
                            <input name="clientes[logradouro]" class="form-control" type="text" id="logradouro"  value="{{$clientes->logradouro}}" disabled />
                        </div>
                    </div>

                    <div class="col-md-1">
                        <div class="form-group">
                            <label>Número*</label>
                            <input value="{{$clientes->numero}}" name="clientes[numero]" class="form-control" disabled>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Complemento</label>
                            <input name="clientes[complemento]" value="{{$clientes->complemento}}" class="form-control" disabled>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Bairro*</label>
                            <input name="clientes[bairro]" value="{{$clientes->bairro}}" id="bairro" class="form-control" type="text" size="40" disabled/>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Cidade*</label>
                            <input name="clientes[cidade]" value="{{$clientes->cidade}}" type="text" class="form-control" id="cidade" size="40" disabled=""/>
                        </div>
                    </div>

                    <div class="col-md-1">
                        <div class="form-group">
                            <label>Estado*</label>
                            <input name="clientes[uf]" value="{{$clientes->uf}}" type="text" class="form-control" id="uf" size="2" disabled=""/>
                        </div>
                    </div>

                    {{--OBSERVACAO--}}
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Observações</label>
                            <textarea class="form-control" name="clientes[observacao]" id="textarea" disabled>{{$clientes->observacao}}</textarea>
                        </div>
                    </div>


                    <input type="hidden" name="status" value="1">
                    <div class="col-md-12 text-right">
                        {{ csrf_field() }}
                        <a href="{{url('clientes')}}">
                            <button type="button" class="btn btn-default">Voltar</button>
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection