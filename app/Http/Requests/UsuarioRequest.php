<?php

namespace App\Http\Requests;

use App\Models\Utils;
use Illuminate\Foundation\Http\FormRequest;

class UsuarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'usuario.nome' => 'required',
            'usuario.idtipo_usuario' => 'required|numeric',
            'usuario.email' => 'required',
            'usuario.senha' => 'nullable'

        ];
    }

    public function messages()
    {
        return [
            'usuario.nome.required' => 'O campo "Nome" é obrigatório',
            'usuario.idtipo_usuario.required' => 'O campo "Tipo de Usuário" é obrigatório',
            'usuario.email.required' => 'O campo "E-mail" é obrigatório',
            'usuario.senha.required' => 'O campo "Senha" é obrigatório'
        ];
    }
}
