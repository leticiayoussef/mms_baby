<?php

namespace App\Http\Controllers;

//use App\Helpers\LogActivity;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function __construct(Request $request)
    {
        Log::info($request->getMethod() . ': ' . $request->getRequestUri(), [
            'data' => $request->all(),
            'method' => $request->getMethod(),
            'IP' => $_SERVER['REMOTE_ADDR'],
        ]);

//        if (isset(Auth::user()->id)) {
//            LogActivity::addToLog(Auth::user()->id, $request);
//        }

    }

}
