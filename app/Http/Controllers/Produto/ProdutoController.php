<?php

namespace App\Http\Controllers\Produto;

use App\DataTablesReceiver;
use App\Models\Produto\Produto;
use App\Models\TipoTamanho;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProdutoController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->middleware('auth');
    }

    public function index()
    {
        return view('produto.listagem');
    }

    public function listarJson(Request $request)
    {
        $instance = Produto::where('produto.status', 1);
        $produto = DataTablesReceiver::setInstance($instance)->setParameters(['idproduto','codigo_produto', 'descricao','valor'])->setConds([['produto.status', '=', '1']])->setRequest($request)->get();

        echo $produto;
    }

    public function frmAdd()
    {
        return view('produto.form')->with([
            'produto' => new Produto(),
            'tipo_tamanho' => TipoTamanho::where('status','=','1')->get(),
        ]);
    }

    public function adicionar(Request $request)
    {
        $produto = $request->post('produto');
        $produto['idusuario'] = \Auth::user()->id;
        $produto['valor_unitario'] = str_replace(',','.', str_replace('.','', $produto['valor_unitario']));
        $produto['valor_venda'] = str_replace(',','.', str_replace('.','', $produto['valor_venda']));
        $produto['status'] = 1;

        $produto = Produto::create($produto);

        if ($produto) {
            return redirect('produtos')

                ->with('success', "Produto cadastrado com sucesso");
        } else {
            return back()->with('error', "Erro ao finalizar o cadastro de Produto");
        }
    }

    public function frmEditar($idproduto)
    {
        $produto = Produto::where([['status', '=', 1],['idproduto', $idproduto]])->first();


        return view('produto.form')->with([
            'produto' => $produto,

        ]);
    }

    public function editar($idproduto, Request $request)
    {
        $produto = $request->post('produto');
        $produto['valor_unitario'] = str_replace(',','.', str_replace('.','', $produto['valor_unitario']));
        $produto['valor_venda'] = str_replace(',','.', str_replace('.','', $produto['valor_venda']));
        $produto['status'] = 1;

        $rProduto = Produto::where('idproduto', '=', $produto['idproduto'])->update($produto);

        if ($rProduto) {

            return redirect('produto')
                ->with('success', "Produto editado com sucesso");

        } else {

            return back()->with('error', "Erro ao editar Produto");
        }
    }

//    public function detalhar($produto)
//    {
//        $produto = Produto::where([['status', '=', 1],['idproduto', $produto]])->first();
//
//        return view('produto.detalhes')->with([
//            'produto' => $produto
//
//        ]);
//    }

    public function deletar(Request $request)
    {
        $produto = $request->validate([
            'id' => 'required|numeric'
        ]);

        if(Produto::where('id', '=', $produto['idproduto'])->update(['status' => 0])){
            echo json_encode(1);
        }
        else{
            throw new Error();
        }
    }

//    public function buscarProduto($descricao)
//    {
//        $estoque = Produto::with('fornecedor')->where([['descricao', 'like', '%' . $descricao . '%'],['status', '1']])
//            ->orWhere([['codigo_produto', 'like', $descricao . '%'], ['status', '1']])
//            ->limit('15')->get();
//
//        echo json_encode($estoque);
//    }
}
