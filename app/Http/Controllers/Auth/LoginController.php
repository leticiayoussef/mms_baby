<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
//use App\Logs\LogActivity;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
//use Spatie\Activitylog\Models\Activity;
use Symfony\Component\HttpFoundation\Request;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $user = User::where('email', $request->post('email'))
            ->where('senha', md5($request->post('password')))
            ->where('status', 1)
            ->first();

        if ($user) {

            Auth::login($user);

            return redirect()->intended('home');

        } else {
            return back()->with('error', 'Erro, usuário ou senha incorretos');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }

}
