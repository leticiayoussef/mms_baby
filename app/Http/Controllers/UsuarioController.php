<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Models\Usuario;
use App\Models\TipoUsuario;
use App\DataTablesReceiver;
use App\Http\Requests\UsuarioRequest;

class UsuarioController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->middleware('auth');

    }

    public function index()
    {
        return view('usuarios.listagem');
    }

    public function listarJson(Request $request)
    {
        $instance = Usuario::with('tipos_usuarios')->where('status',1);

        echo DataTablesReceiver::setInstance($instance)->setParameters(['id','nome', 'email','idtipo_usuario'])->setRequest($request)->get();
    }

    public function frmAdd()
    {
        return view('usuarios.form')->with([
            'usuario' => new Usuario(),
            'tipos_usuarios' => TipoUsuario::where('status', 1)->get()
        ]);
    }

    public function adicionar(UsuarioRequest $request)
    {
        $usuario = $request->post('usuario');

        $usuario['senha'] = md5($usuario['senha']);

        /*buscar usuario antes de salvar para que nao haja emails duplicados*/
        $usuariosExistentes = $this->buscarUsuarioPorEmail($usuario['email']);

        if ($usuariosExistentes->count() > 0) {
            return back()->with($usuario)->with('error', "Erro! Esse e-mail já pert ao cadastrar o usuario, já existe esse e-mail.");
        }

        if (Usuario::create($usuario)->save()) {
            return redirect('usuarios')->with('success', "Usuario cadastrado com sucesso");
        } else {
            return back()->with('error', "Erro ao cadastrar o usuario");
        }
    }

    public function frmEditar($id)
    {
        $usuario = Usuario::with('tipos_usuarios')->findOrFail($id);

        return view('usuarios.form')->with([
            'usuario' => $usuario,
            'tipos_usuarios' => TipoUsuario::where([['status', 1]])->get()
        ]);
    }

    public function editar(UsuarioRequest $request)
    {
        $usuario = $request->post('usuario');

        $rUsuario = Usuario::where('id', $usuario['id'])->update($usuario);

        if ($rUsuario) {
            return redirect('usuarios')->with('success', "Usuário editado com sucesso");
        } else {
            return back()->with('error', "Erro ao editar o usuário");
        }
    }

    public function deletar(Request $request)
    {
        $usuario = $request->validate([
            'id' => 'required|numeric'
        ]);

        if(Usuario::where('id', '=', $usuario['id'])->update(['status' => 0])){
            echo json_encode(1);
        }
        else{
            throw new Error();
        }

    }

    public function detalhar($id)
    {
        $usuario = Usuario::with('tipos_usuarios')->findOrFail($id);

        return view('usuarios.detalhes')->with([
            'usuario' => $usuario,
        ]);
    }

    public function resetarSenha(Request $request)
    {
        $usuario = $request->validate([
            'id' => 'required|numeric',
            'senha' => 'required'
        ]);

        $novaSenha = md5($usuario['senha']);

        $usuario = Usuario::where('id', $usuario['id'])->update([
            'senha' => $novaSenha
        ]);

        echo json_encode('Senha alterada com sucesso');
    }

    public function buscarUsuarioPorEmail($email)
    {
        return $usuario = Usuario::where('email', $email)->get();
    }
}
