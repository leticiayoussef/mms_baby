<?php

namespace App\Http\Controllers\Fornecedor;

use App\DataTablesReceiver;
use App\Models\Fornecedor\Fornecedor;
use App\Models\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FornecedorController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->middleware('auth');
    }

    public function index()
    {
        return view('fornecedor.listagem');
    }

    public function listarJson(Request $request)
    {
        $instance = Fornecedor::where('fornecedor.status', 1);
        $fornecedor = DataTablesReceiver::setInstance($instance)
            ->setParameters(['razao_social', 'nome_fantasia', 'cnpj', 'telefone'])
            ->setConds('fornecedor.status', '=', '1')
            ->setRequest($request)->get();

        echo $fornecedor;
    }

    public function frmAdd()
    {
        return view('fornecedor.form')->with([
            'fornecedor' => new Fornecedor(),
        ]);
    }

    public function adicionar(Request $request)
    {
        $fornecedor = $request->post('fornecedor');
        $fornecedor['status'] = 1;

        $fornecedor = Fornecedor::create($fornecedor);

        if ($fornecedor) {
            return redirect('fornecedor')->with('success', "Fornecedor cadastrado com sucesso");
        } else {
            return back()->with('error', "Erro ao finalizar o cadastro de Fornecedor");
        }

    }

    public function checarEmail(Request $request)
    {
        if (Fornecedor::where([['email', $request->post('email')], ['idfornecedor', '<>', $request->post('$idfornecedor')], ['status', '=', 1]])->count() > 0) {
            echo json_encode(1);
        } else {
            echo json_encode(0);
        }
    }

    public function frmEditar($idfornecedor)
    {
        $fornecedor = Fornecedor::where([['status', '=', 1],['idfornecedor', $idfornecedor]])->first();
        return view('fornecedor.form')->with([
            'fornecedor' => $fornecedor,

        ]);
    }

    public function editar($idfornecedor, Request $request)
    {
        //fornecedor
        $fornecedor = $request->post('fornecedor');


        $rFornecedor = Fornecedor::where('$idfornecedor', '=', $fornecedor['$idfornecedor'])->update($fornecedor);

        if ($rFornecedor) {
            return redirect('fornecedor')->with('success', "Fornecedor editado com sucesso");
        } else {
            return back()->with('error', "Erro ao editar Fornecedor");
        }
    }

    public function detalhar($idfornecedor)
    {
        $fornecedor = Fornecedor::where([['status', '=', 1],['$idfornecedor', $idfornecedor]])->first();

        return view('fornecedor.detalhes')->with([
            'fornecedor' => $fornecedor
        ]);
    }

    public function deletar($fornecedor, Request $request)
    {
        if (Fornecedor::where('$idfornecedor', '=', $fornecedor)->update(['status' => 0])) {
            echo json_encode(1);
        } else {
            throw new Error();
        }
    }

    public function buscarFornecedor($descricao)
    {
        $fornecedor = Fornecedor::where([['razao_social', 'like', '%' . $descricao . '%'],['status', '1']])
            ->orWhere([['cnpj', 'like', $descricao . '%'], ['status', '1']])
            ->orWhere([['nome_fantasia', 'like', $descricao . '%'], ['status', '1']])
            ->limit('15')->get();

        echo json_encode($fornecedor);

    }

}
