<?php

namespace App\Http\Controllers\Produto;

use App\DataTablesReceiver;
use App\Models\TipoTamanho;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TipoTamanhoController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->middleware('auth');
    }

    public function index()
    {
        return view('configuracoes.tipo_tamanho.listagem');
    }

    public function listarJson(Request $request)
    {
        $instance = Produto::where('tipo_tamanho.status', 1);
        $tipo_tamanho = DataTablesReceiver::setInstance($instance)
            ->setParameters(['idtipo_tamanho','descricao'])
            ->setConds(['tipo_tamanho.status', '=', '1'])
            ->setRequest($request)
            ->get();

        echo $tipo_tamanho;
    }

    public function frmAdd()
    {
        return view('configuracao.tipo_tamanho.form')->with([
            'tipo_tamanho' => new TipoTamanho(),
        ]);
    }

    public function adicionar(Request $request)
    {
        $tipo_tamanho = $request->post('tipo_tamanho');
        $tipo_tamanho['status'] = 1;

        $tipo_tamanho = TipoTamanho::create($tipo_tamanho);

        if ($tipo_tamanho) {
            return redirect('tipo_tamanho')

                ->with('success', "Tipo de Tamanho cadastrado com sucesso");
        } else {
            return back()->with('error', "Erro ao finalizar o cadastro de Tipo de Tamanho");
        }
    }

    public function frmEditar($idtipo_tamanho)
    {
        $tipo_tamanho = TipoTamanho::where([['status', '=', 1],['idtipo_tamanho', $idtipo_tamanho]])->first();


        return view('configuracao.tipo_tamanho.form')->with([
            'tipo_tamanho' => $tipo_tamanho,

        ]);
    }

    public function editar($idtipo_tamanho, Request $request)
    {
        $tipo_tamanho = $request->post('tipo_tamanho');
        $tipo_tamanho['status'] = 1;

        $rTipo_tamanho = TipoTamanho::where('idtipo_tamanho', '=', $tipo_tamanho['idtipo_tamanho'])->update($tipo_tamanho);

        if ($rTipo_tamanho) {

            return redirect('tipo_tamanho')
                ->with('success', "Tipo de Tamanho editado com sucesso");

        } else {

            return back()->with('error', "Erro ao editar Tipo de Tamanho");
        }
    }

    public function detalhar($tipo_tamanho)
    {
        $tipo_tamanho = TipoTamanho::where([['status', '=', 1],['idtipo_tamanho', $tipo_tamanho]])->first();

        return view('configuracao.tipo_tamanho.detalhes')->with([
            'tipo_tamanho' => $tipo_tamanho

        ]);
    }

    public function deletar(Request $request)
    {
        $tipo_tamanho = $request->validate([
            'idtipo_tamanho' => 'required|numeric'
        ]);

        if(TipoTamanho::where('idtipo_tamanho', '=', $tipo_tamanho['idtipo_tamanho'])
            ->update(['status' => 0])){
            echo json_encode(1);
        }
        else{
            throw new Error();
        }
    }
}
