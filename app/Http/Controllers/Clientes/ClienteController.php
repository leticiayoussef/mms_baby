<?php

namespace App\Http\Controllers\Clientes;

use App\DataTablesReceiver;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Clientes\Clientes;
use App\Models\Clientes\ClienteEnderecos;

class ClienteController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->middleware('auth');
    }

    public function index()
    {
        return view('clientes.listagem');
    }

    public function listarJson(Request $request)
    {
        $instance = Clientes::where('clientes.status', 1);
        $clientes = DataTablesReceiver::setInstance($instance)
            ->setParameters(['nome', 'razao_social', 'cnpj_cpf', 'telefone'])->setConds([['clientes.status', '=', '1']])
            ->setRequest($request)->get();

        echo $clientes;
    }

    public function frmAdd()
    {
        return view('clientes.form')->with([
            'clientes' => new Clientes(),
        ]);
    }

    public function adicionar(Request $request)
    {
        $clientes = $request->post('clientes');

        $clientes = Clientes::create($clientes);


        if ($clientes) {
            return redirect('clientes')->with('success', "Cliente cadastrado com sucesso");
        } else {
            return back()->with('error', "Erro ao finalizar o cadastro de Cliente");
        }

    }

    public function frmEditar($idcliente)
    {
        $clientes = Clientes::where([['status', '=', 1],['idcliente', $idcliente]])->first();
        return view('clientes.form')->with([
            'clientes' => $clientes

        ]);
    }

    public function editar(Request $request)
    {

        $clientes = $request->post('clientes');
        $dados['nome_contato'] = $request->post('nome_contato');
        $dados['cnpj_cpf'] = $request->post('cnpj_cpf');
        $dados['rg'] = $request->post('rg');
        $dados['data_nascimento'] = $request->post('data_nascimento');


        $rCliente = Clientes::where('idcliente', '=', '1')->update($clientes);

        if ($rCliente) {
            return redirect('clientes')->with('success', "Cliente editado com sucesso");
        } else {
            return back()->with('error', "Erro ao editar Cliente");
        }
    }

    public function detalhar($idcliente)
    {
        $clientes = Clientes::where([['status', '=', 1],['idcliente', $idcliente]])->first();

        return view('clientes.detalhes')->with([
            'clientes' => $clientes
        ]);
    }

    public function deletar($clientes, Request $request)
    {
        $clientes = $request->validate([
            'idcliente' => 'required|numeric'
        ]);

        if(Clientes::where('id', '=', $clientes['idcliente'])->update(['status' => 0])){
            echo json_encode(1);
        }
        else{
            throw new Error();
        }
    }

}
