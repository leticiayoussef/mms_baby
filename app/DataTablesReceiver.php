<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Mixed_;

/**
 * Class DataTablesReceiver
 * @package App
 */
class DataTablesReceiver extends Model
{
    /** @var Object $instance Recebe um objeto da classe que deverá ser listada */
    private $instance;

    /** @var Request $request Recebe um objeto da classe Request do Laravel */
    private $request;

    /** @var Int $limit O limite de linhas que será lido do banco */
    private $limit;

    /** @var Int $total Total de linhas no banco */
    private $total;

    /** @var mixed[] $parameters Array contendo o nome dos campos que devem ser filtrados na tabela */
    private $parameters;

    /** @var mixed[] $conds Array contendo condições pré definidas para a query */
    private $conds;

    /** @var String $indiceFiltragem String que substitui o indice padrão que é passado pelo DataTables para filtragem*/
    private $indiceFiltragem = 'value';

    private $textConditions;

    /**
     * Método que recebe um objeto da classe que deverá ser listada
     *
     * @param $ormClass Object Objeto da classe que deverá ser listada
     *
     * @return DataTablesReceiver Nova instância da classe DataTablesReceiver
     */
    public static function setInstance($ormClass){
        $obj = new Self;
        $obj->instance = $ormClass;
//        $obj->instance = new $ormClass;
        return $obj;
    }

    /**
     * Seta o nome dos campos da tabela que serão usados como filtros posteriormente
     *
     * @param mixed[] $array Array contendo o nome dos campos que devem ser filtrados na tabela
     *
     * @return DataTablesReceiver Instancia da classe DataTablesReceiver
     */
    public function setParameters($array){
        $this->parameters = $array;
        return $this;
    }

    /**
     * Seta o indice de filtragem
     *
     * @param mixed[] String String que substitui o indice padrão que é passado pelo DataTables para filtragem
     *
     * @return DataTablesReceiver Instancia da classe DataTablesReceiver
     */
    public function setIndiceFiltragem($string){
        $this->indiceFiltragem = $string;
        return $this;
    }

    /**
     * Seta o as condições préd definidas para a query
     *
     * @param mixed[] $array Array contendo condições pré definidas para a query
     *
     * @return DataTablesReceiver Instancia da classe DataTablesReceiver
     */
    public function setConds($array){
        $this->conds = $array;
        return $this;
    }

    /**
     * Recebe um objeto da classe Request do Laravel
     *
     * @param Request $request Objeto da classe Request
     *
     * @return DataTablesReceiver Instância da classe DataTablesReceiver
     */
    public function setRequest(Request $request){
        $this->request = $request;

        $this->filter();

        return $this;
    }

    /**
     * Faz a filtragem, a listagem e a páginação dos dados
     */
    private function filter(){
        $this->limit = $this->request->get('length') ? $this->request->get('length') : 25;
        $offset = $this->request->get('start') ? $this->request->get('start') : 0;

        //caso hajam condições pré definidas
        if(!empty($this->conds)){
            $this->instance = $this->instance->where($this->conds);
        }

        if(isset($this->request->get('search')[$this->indiceFiltragem])) {
            if ($this->request->get('search')[$this->indiceFiltragem]) {
                $like = '%' . $this->request->get('search')[$this->indiceFiltragem] . '%';
                $cond = true;

                //iniciando estes atributos zerados, coloquei-os como atributo pois se ficassem como variável não funcionariam no CALLBACK do where a frente
                $this->textConditions[0] = [];
                $this->textConditions[1] = [];

                //laço de repetição para montar os likes
                foreach ($this->parameters as $p) {
                    if ($cond) {
                        //adiciona condições na query
                        $this->textConditions[0][] = [$p, 'like', $like];
                        $cond = false;
                    } else {
                        //adiciona condições na query
                        $this->textConditions[1][] = [$p, 'like', $like];
                    }
                }

                //faz a segunda condição da query, relacionada com o "LIKE"
                $this->instance = $this->instance->where(function ($query) {
                    $query->where($this->textConditions[0])->orWhere($this->textConditions[1]);
                    $query = $query->where($this->textConditions[0]);

                    foreach ($this->textConditions[1] as $c) {
                        $query->orWhere(array($c));
                    }

                });
            }
        }

        $this->total = $this->instance->count();
        if($this->limit > 0) {
            $this->instance = $this->instance->limit($this->limit)->offset($offset);
        }

        //prepara a ordenação
        $order = $this->request->get('order');
        $columns = $this->request->get('columns');
//        dd($order);
        //monta a ordenação
        $this->instance = $this->instance->orderBy($columns[$order[0]['column']]['name'],$order[0]['dir']);
    }

    /**
     * @return string Json preparado para o datatables
     */
    public function get()
    {
        //dd($this->instance);
        return json_encode([
            'data' => $this->instance->get(),
            'recordsFiltered' => $this->total,
            'recordsTotal' => $this->limit
        ]);
    }

    public function getInstance(){
        return $this->instance;
    }

    public function getTotal(){
        return $this->total;
    }

    public function getLimit(){
        return $this->limit;
    }
}
