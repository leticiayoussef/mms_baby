<?php
/**
 * Verifica se o old('') contem um valor, se nao contiver, mostra o segundo valor passado
 * v1= old('campo')
 * v2= $objeto->atributo provenientes do BD
 */
function inputValue($v1, $v2)
{
    if ($v1) {
        echo $v1;
    } else {
        echo $v2;
    }
}

