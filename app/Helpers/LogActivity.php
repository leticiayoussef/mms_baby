<?php

namespace App\Helpers;
use Request;
use App\Logs\LogActivity as LogActivityModel;

class LogActivity
{

    public static function addToLog($idusuario)
    {
        $log = [];
        $log['subject'] = Request::path();
        $log['url'] = Request::url();
        $log['method'] = Request::method();
        $log['ip'] = Request::ip();
        $log['agent'] = Request::header('user-agent');
        $log['user_id'] = $idusuario;

        LogActivityModel::create($log);


    }
    public static function logActivityLists()
    {
        return LogActivityModel::all()->limit(15)->get();
    }

}