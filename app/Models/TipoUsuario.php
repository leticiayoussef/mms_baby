<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class TipoUsuario extends Model
{
    use Notifiable;
    /**
     * Tabela associada a modelo.
     *
     * @var string
     */
    protected $table = 'tipos_usuarios';

    /**
     * Chave primária da tabela tipos_usuarios.
     *
     * @var string
     */
    protected $primaryKey = 'idtipo_usuario';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idtipo_usuario',
        'descricao',
        'status'
    ];

    const CREATED_AT = 'data_criacao';
    const UPDATED_AT = 'data_atualizacao';
}
