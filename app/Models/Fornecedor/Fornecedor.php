<?php

namespace App\Models\Fornecedor;

use Illuminate\Database\Eloquent\Model;

class Fornecedor extends Model
{
    /**
     * Tabela associada a modelo.
     *
     * @var string
     */
    protected $table = 'fornecedor';

    /**
     * Chave primária da tabela empresa
     *
     * @var string
     */
    protected $primaryKey = 'idforncedor';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idforncedor',
        'razao_social',
        'site',
        'cnpj',
        'inscricao_estadual',
        'email',
        'telefone',
        'logradouro',
        'numero',
        'complemento',
        'cep',
        'bairro',
        'cidade',
        'pais',
        'uf',
        'status'
    ];

    const CREATED_AT = 'data_criacao';
    const UPDATED_AT = 'data_atualizacao';

}