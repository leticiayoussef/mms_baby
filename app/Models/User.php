<?php

namespace App\Models;

//use App\Logs\Logs;
//use App\Logs\NewsItem;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Log;
use Monolog\Handler\FirePHPHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
//use Spatie\Activitylog\Models\Activity;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'usuarios';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'idtipo_usuario', 'nome', 'email', 'senha', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'senha'
    ];

    const CREATED_AT = 'data_criacao';
    const UPDATED_AT = 'data_atualizacao';

    public function tipo(){
        return $this->hasOne('App\TipoUsuario', 'idtipo_usuario', 'idtipo_usuario');
    }

}
