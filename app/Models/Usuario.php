<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable
{
    use Notifiable;

    /**
     * Tabela associada a modelo.
     *
     * @var string
     */
    protected $table = 'usuarios';

    /**
     * Chave primária da tabela usuario.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'idtipo_usuario',
        'nome',
        'email',
        'senha',
        'status'

    ];

    const CREATED_AT = 'data_criacao';
    const UPDATED_AT = 'data_atualizacao';

    public function tipos_usuarios(){
        return $this->hasOne('App\Models\TipoUsuario', 'idtipo_usuario', 'idtipo_usuario');
    }

}
