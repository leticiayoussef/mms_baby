<?php

namespace App\Models\Produto;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{

    /**
     * Tabela associada a modelo.
     *
     * @var string
     */
    protected $table = 'produto';

    /**
     * Chave primária da tabela empresa
     *
     * @var string
     */
    protected $primaryKey = 'idproduto';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idproduto',
        'idtipo_tamanho',
        'codigo_produto',
        'descricao',
        'valor_unitario',
        'valor_venda',
        'estoque_minimo',
        'status'
    ];

    const CREATED_AT = 'data_criacao';
    const UPDATED_AT = 'data_atualizacao';

    public function tipo_tamanho(){
        return $this->hasOne('App\Models\TipoTamanho', 'idtipo_tamanho', 'idtipo_tamanho');
    }

}
