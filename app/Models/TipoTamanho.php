<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class TipoTamanho extends Authenticatable
{
    use Notifiable;

    /**
     * Tabela associada a modelo.
     *
     * @var string
     */
    protected $table = 'tipo_tamanho';

    /**
     * Chave primária da tabela usuario.
     *
     * @var string
     */
    protected $primaryKey = 'idtipo_tamanho';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idtipo_tamanho',
        'descricao',
        'status'

    ];

    const CREATED_AT = 'data_criacao';
    const UPDATED_AT = 'data_atualizacao';
}
