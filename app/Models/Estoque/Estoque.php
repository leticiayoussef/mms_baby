<?php

namespace App\EstoqueProduto;

use Illuminate\Database\Eloquent\Model;

class Estoque extends Model
{
    /**
     * Tabela associada a modelo.
     *
     * @var string
     */
    protected $table = 'estoque';

    /**
     * Chave primária da tabela empresa
     *
     * @var string
     */
    protected $primaryKey = 'idestoque';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idestoque',
        'idproduto',
        'data_entrada',
        'quantidade',
        'idusuario_responsavel_entrada',
        'valor',
        'total',
        'status'
    ];

    const CREATED_AT = 'data_criacao';
    const UPDATED_AT = 'data_atualizacao';


    public function produto()
    {
        return $this->hasOne('App\EstoqueProduto\Produto', 'idproduto','idproduto')->with('fornecedor')->with('tipoProduto')->with('unidadeMedida');
    }

    public function responsavel_entrada()
    {
        return $this->hasOne('App\Usuario', 'id','idusuario_responsavel_entrada');
    }

}
