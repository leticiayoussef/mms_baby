<?php

namespace App\Models\Clientes;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ClienteEnderecos extends Authenticatable
{
    use Notifiable;

    /**
     * Tabela associada a modelo.
     *
     * @var string
     */
    protected $table = 'cliente_enderecos';

    /**
     * Chave primária da tabela usuario.
     *
     * @var string
     */
    protected $primaryKey = 'idcliente_enderecos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idcliente_endereco',
        'idcliente_tipo_endereco',
        'idcliente',
        'logradouro',
        'numero',
        'complemento',
        'cep',
        'bairro',
        'cidade',
        'pais',
        'uf',
        'status'

    ];

    const CREATED_AT = 'data_criacao';
    const UPDATED_AT = 'data_atualizacao';

    public function tipo_endereco(){
        return $this->hasOne('App\Clientes\ClienteTipoEnderecos', 'idcliente_tipo_endereco', 'idcliente_tipo_endereco');
    }

    public function cliente(){
        return $this->hasOne('App\Clientes\Clientes', 'idcliente', 'idcliente');
    }
}

