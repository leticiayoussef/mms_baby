<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ClienteTiposEnderecos extends Authenticatable
{
    use Notifiable;

    /**
     * Tabela associada a modelo.
     *
     * @var string
     */
    protected $table = 'cliente_tipos_enderecos';

    /**
     * Chave primária da tabela usuario.
     *
     * @var string
     */
    protected $primaryKey = 'idcliente_tipo_endereco';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idcliente_tipo_endereco',
        'descricao',
        'status'

    ];

    const CREATED_AT = 'data_criacao';
    const UPDATED_AT = 'data_atualizacao';
}

