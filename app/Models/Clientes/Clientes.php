<?php

namespace App\Models\Clientes;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Clientes extends Authenticatable
{
    use Notifiable;

    /**
     * Tabela associada a modelo.
     *
     * @var string
     */
    protected $table = 'clientes';

    /**
     * Chave primária da tabela usuario.
     *
     * @var string
     */
    protected $primaryKey = 'idcliente';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idcliente',
        'nome',
        'email',
        'telefone',
        'celular',
        'cep',
        'logradouro',
        'numero',
        'complemento',
        'bairro',
        'cidade',
        'uf',
        'observacao',
        'status'

    ];

    const CREATED_AT = 'data_criacao';
    const UPDATED_AT = 'data_atualizacao';

}

