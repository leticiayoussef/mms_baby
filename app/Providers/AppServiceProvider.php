<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Illuminate\Support\Facades\Schema::defaultStringLength(191);

        /**
         *  Verifica se o old() contem um valor, se nao contiver mostra o segundo valor passado
         *  Ex: @inputValue(old('descricao'), $escola->descricao)
         * */
        Blade::directive('inputValue', function ($old) {
            return "<?php echo App\Models\Utils::inputValue($old); ?>";
        });


        /**
         *  Verifica se o old() contem um valor, se nao contiver mostra o segundo valor passado
         *  Ex: @brenda(old('descricao'), $escola->descricao)
         * */
//        Blade::directive('brenda', function ($b) {
/*            return "<?php echo App\Models\Utils::inputValue($b); ?>";*/
//        });


        /**
         *  Faz as options de um select
         *  Ex: @options($rows, value, descricao, selecionado)
         * */
        Blade::directive('options', function ($options) {
            return "<?php echo App\Models\Utils::options($options); ?>";
        });
    }

}
