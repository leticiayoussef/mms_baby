$("#idpais").change(function () {
    $.ajax({
        url: BASE_URL+'pais/'+$("#idpais").val()+'/estado',
        type: 'GET',
        dataType: 'JSON',
        success: function(result){
            html = '<option>Selecione um estado</option>' +
                '<option value="">Outro</option>';


            $(result).each(function(i, field){
                html += '<option value="'+field.iduf+'">'+field.descricao+'</option>';
            });

            $("#iduf").html(html);
        }
    });
});
$("#iduf").change(function () {
    $.ajax({
        url: BASE_URL+'uf/'+$("#iduf").val()+'/cidades',
        type: 'GET',
        dataType: 'JSON',
        success: function(result){
            html = '<option>Selecione uma cidade</option>' +
                '<option value="">Outro</option>';

            $(result).each(function(i, field){
                html += '<option value="'+field.idcidade+'">'+field.descricao+'</option>';
            });

            $("#idcidade").html(html);
        }
    });
});

$("#idcidade").change(function () {
    $.ajax({
        url: BASE_URL+'cidades/'+$("#idcidade").val()+'/bairros',
        type: 'GET',
        dataType: 'JSON',
        success: function(result){
            html = '<option>Selecione um bairro</option>' +
                '<option value="">Outro</option>';

            $(result).each(function(i, field){
                html += '<option value="'+field.idbairro+'">'+field.descricao+'</option>';
            });

            $("#idbairro").html(html);
        }
    });
});