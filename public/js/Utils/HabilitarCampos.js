$('#idpais').on('change', function() {
    var tipo = $(this).find('option:selected').text().trim();
    if(tipo == 'Outro') {
        $("#novoPais").show();
    }
    else{
        $("#novoPais").hide();
    }
});


$('#iduf').on('change', function() {
    var tipo = $(this).find('option:selected').text().trim();
    if(tipo == 'Outro') {
        $("#novoEstado").show();
    }
    else{
        $("#novoEstado").hide();
    }
});

$('#idcidade').on('change', function() {
    var tipo = $(this).find('option:selected').text().trim();
    if(tipo == 'Outro') {
        $("#novaCidade").show();
    }
    else{
        $("#novaCidade").hide();
    }
});

$('#idbairro').on('change', function() {
    var tipo = $(this).find('option:selected').text().trim();
    if(tipo == 'Outro') {
        $("#novoBairro").show();
    }
    else{
        $("#novoBairro").hide();
    }
});



