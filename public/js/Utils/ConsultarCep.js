$(document).ready(function(){

    if($("#endereco_no_exterior").prop("checked")){
        $("#cep").unmask();
    }
    else{
        $("#cep").mask("00000-000");
    }


    $("#cep").blur(function () {
        if (!$("#endereco_no_exterior").prop("checked")) {
            $("#lblcep").show();
            $("#codigoPostal").hide();
            $("#pais").attr("placeholder", "Brasil");
            $("#pais").attr("value", "Brasil");

            //Consulta o webservice viacep.com.br/
            $.getJSON("https://viacep.com.br/ws/"+ $("#cep").val() +"/json/?callback=?", function(dados) {
                if (!("erro" in dados)) {
                    //Atualiza os campos com os valores da consulta.
                    $("#logradouro").val(dados.logradouro);
                    $("#bairro").val(dados.bairro);
                    $("#uf").val(dados.uf);
                    $("#cidade").val(dados.localidade);
                    $("#pais").val("Brasil");
                }
                else{
                    swal('Aviso, CEP não encontrado', 'Verifique ou digite o endereço manualmente', 'warning');
                }
            });
        }
    });
    //
    $("#endereco_no_exterior").change(function(){

        if($("#endereco_no_exterior").prop("checked")){
            $("#cep").unmask();
            $("#lblcep").hide();
            $("#lblcodigopostal").show();
            $("#codigoPostal").show();
            // $("#pais").attr("value", "");
        }
        else{
            $("#cep").mask("00000-000");
            $("#lblcep").show();
            $("#codigoPostal").hide();
            $("#lblcodigopostal").hide();

        }
    });

    $("#cpf").mask("999.999.999-99");
    $("#data_nascimento").mask("99/99/9999");

});
